import random

def main():
  print("================================================")
  print("   ¡Bienvenido al juego de adivina el número!   ")
  print("================================================")
  print("Tu objetivo es adivinar un número generado por el ordenador.")
  
  numeroAleatorio = random.randint(1, 100)
  
  prediccion = 0
  intentos = 0
  
  while prediccion != numeroAleatorio:
    intentos += 1
    
    prediccion = int(input("Intenta adivinar el número generado entre 1 y 100: "))
    
    if prediccion > numeroAleatorio:
      print("El número elegido es demasiado alto")
      continue
    elif prediccion < numeroAleatorio:
      print("El número elegido es demasiado bajo")
      continue
    else:
      print(f"¡Has acertado despues de {intentos} intentos!")

if __name__ == '__main__':
  main()