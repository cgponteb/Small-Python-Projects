# Creation of a Mad Libs game with f-strings

print("Bienvenido al juego Mad Libs (Historias Locas)!\n\nPara empezar, te pediré algunas palabras:")

adjetivo = input("Introcude un adjetivo: ")
verbo1 = input("Introduce un verbo: ")
verbo2 = input("Introduce otro verbo distinto: ")
sustantivo_plural = input("Introduce un sustantivo en plural: ")

madlib = f"¡Programar es tan {adjetivo}! Siempre me emociona porque me encanta {verbo1} problemas. ¡Aprende a {verbo2} con freeCodeCamp y alcanza tus {sustantivo_plural}!"

print("\n¡Aquí tienes tu historia!\n")
print(madlib)